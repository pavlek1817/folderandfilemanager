﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.BusinessLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FolderAndFileExplorer.Web.Controllers
{
    public class BaseApiController : ControllerBase
    {
        protected IUnitOfWork UnitOfWork { get; private set; }
        protected IFileAndFolderBusinessLogic FileAndFolderBusinessLogic { get; private set; }
        protected ILogger Logger { get; private set; }

        public BaseApiController(IUnitOfWork unitOfWork, IFileAndFolderBusinessLogic fafBusinessLogic, ILogger logger)
        {
            UnitOfWork = unitOfWork;
            FileAndFolderBusinessLogic = fafBusinessLogic;
            Logger = logger;
        }
    }
}
