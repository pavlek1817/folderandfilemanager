﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.BusinessLogic;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using FolderAndFileExplorer.Core.Models;
using FolderAndFileExplorer.Web.Models.FolderModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace FolderAndFileExplorer.Web.Controllers
{
    [Route("api/folder")]
    [ApiController]
    public class FolderController : BaseApiController
    {
        public FolderController(IUnitOfWork uow, IFileAndFolderBusinessLogic fafBusinessLogic, ILogger<FolderController> logger)
            : base(uow, fafBusinessLogic, logger)
        {
        }

        /// <summary>
        /// Method that creates new folder in specified folder. If folder is not specified then it creates in root
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create(FolderCreateBindingModel model)
        {
            Logger.LogInformation($"Creating folder with name {model.Name} ...");
            string parentFolderPath = string.Empty;
            

            var transaction = UnitOfWork.BeginTransaction();

            try
            {
                // If folder is set, then find its relative path
                if (model.FolderId.HasValue)
                {
                    parentFolderPath = UnitOfWork.FolderRepository.GetById(model.FolderId.Value)
                        .Path;
                }

                var folderPath = Path.Combine(parentFolderPath, model.Name);

                // Create folder in database
                var folder = model.GetFolder(folderPath);
                UnitOfWork.FolderRepository.Add(folder);
                UnitOfWork.SaveChanges();

                // Calculate folder path and save folder

                FileAndFolderBusinessLogic.CreateFolder(folderPath);

                transaction.Commit();

                Logger.LogInformation($"Folder with name {model.Name} successfully created!");
                return Ok(new FolderGetViewModel(folder));
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                Logger.LogError($"Unexpected error occured when creating folder with name {model.Name}! Exception details: {ex}");
                throw;
            }
        }

        /// <summary>
        /// Method that deletes existing folder recursively
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            Logger.LogInformation($"Deleting folder with id {id} ...");

            if (!UnitOfWork.FolderRepository.CheckIfExists(id))
            {
                Logger.LogWarning($"Trying to delete folder with id {id} that doesn't exists!");
                return BadRequest($"Folder with id {id} doesnt exists!");
            }

            var file = UnitOfWork.FolderRepository.GetById(id);

            var transaction = UnitOfWork.BeginTransaction();

            try
            {
                // Delete folder in database
                UnitOfWork.FolderRepository.Delete(id);
                UnitOfWork.SaveChanges();

                // Delete file from file system
                FileAndFolderBusinessLogic.DeleteFolder(file.Path);

                transaction.Commit();
                Logger.LogInformation($"Folder with id {id} and path {file.Path} deleted successfully!");
                return Ok();
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                Logger.LogError($"Unexpected error occured when deleting folder with id {id}! Exception details: {ex}");
                throw;
            }
        }

        /// <summary>
        /// Method that gets folder
        /// </summary>
        /// <param name="id">Specified folder id</param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public ActionResult<FolderGetViewModel> GetById(int id)
        {
            try
            {
                return new FolderGetViewModel(UnitOfWork.FolderRepository.GetById(id));
            }
            catch (Exception ex)
            {
                Logger.LogError($"Unexpected error occured when geting folder with id {id}! Exception details: {ex}");
                throw;
            }
        }

        /// <summary>
        /// Method that gets all folder sub items (files and folders) for specified folder id
        /// </summary>
        /// <param name="id">Specified folder id, if is null, then method returns root folder items</param>
        /// <returns></returns>
        [HttpGet("get-subitems")]
        public ActionResult<ICollection<FileAndFolderGetViewModel>> GetFolderItems(int? id)
        {
            try
            {
                return Ok(UnitOfWork.FolderRepository.GetFolderItems(id));
            }
            catch (Exception ex)
            {
                Logger.LogError($"Unexpected error occured when geting folder items with id {id}! Exception details: {ex}");
                throw;
            }
        }

    }
}
