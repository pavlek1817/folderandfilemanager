﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.BusinessLogic;
using FolderAndFileExplorer.Core.Queries;
using FolderAndFileExplorer.Web.Models.FileModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace FolderAndFileExplorer.Web.Controllers
{
    [Route("api/file")]
    [ApiController]
    public class FileController : BaseApiController
    {
        private const string DEDICATED_EXTENSION = ".txt";
        public FileController(IUnitOfWork uow, IFileAndFolderBusinessLogic fafBusinessLogic, ILogger<FileController> logger)
            : base(uow, fafBusinessLogic, logger)
        {
        }


        /// <summary>
        /// Method that gets file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public ActionResult<FileGetViewModel> GetById(int id)
        {
            try
            {
                return new FileGetViewModel(UnitOfWork.FileRepository.GetById(id));
            }
            catch(Exception ex)
            {
                Logger.LogError($"Unexpected error occured when geting file with id {id}! Exception details: {ex}");
                throw;
            }
        }

        /// <summary>
        /// Method that creates new file in specified folder. If folder is not specified then it creates in root
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create(FileCreateBindingModel model)
        {
            Logger.LogInformation($"Creating file with name {model.Name} ...");
            string folderPath = string.Empty;
            // If folder is set, then find its relative path
            if (model.FolderId.HasValue)
            {
                folderPath = UnitOfWork.FolderRepository.GetById(model.FolderId.Value)
                    .Path;
            }

            var filePath = Path.Combine(folderPath, $"{model.Name}{DEDICATED_EXTENSION}");

            var transaction = UnitOfWork.BeginTransaction();

            try
            {

                // Create file in database
                var file = model.GetFile(filePath);
                UnitOfWork.FileRepository.Add(file);
                UnitOfWork.SaveChanges();

                // Calculate file path and save file
                FileAndFolderBusinessLogic.CreateFile(filePath);

                transaction.Commit();

                Logger.LogInformation($"File with name {file.Name} and id {file.Id} successfully created!");
                return Ok(new FileGetViewModel(file));
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                Logger.LogError($"Unexpected error occured when creating file with name {model.Name}! Exception details: {ex}");
                throw;
            }
            
        }

        /// <summary>
        /// Method that deletes existing file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            Logger.LogInformation($"Deleting file with id {id} ...");

            if (!UnitOfWork.FileRepository.CheckIfExists(id))
            {
                Logger.LogWarning($"Trying to delete file with id {id} that doesn't exists!");
                return BadRequest($"File with id {id} doesnt exists!");
            }

            var file = UnitOfWork.FileRepository.GetById(id);

            var transaction = UnitOfWork.BeginTransaction();

            try
            {
                // Remove file from database
                UnitOfWork.FileRepository.Delete(id);
                UnitOfWork.SaveChanges();

                // Remove file from file system
                FileAndFolderBusinessLogic.DeleteFile(file.RelativePath);

                transaction.Commit();
                Logger.LogInformation($"File with id {id} and name {file.Name} deleted successfully!");
                return Ok();
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                Logger.LogError($"Unexpected error occured when deleting file with id {id}! Exception details: {ex}");
                throw new Exception();
            }
        }

        /// <summary>
        /// Method that searches file by exact name and folder id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("search-by-name")]
        public ActionResult<FileGetViewModel> SearchByName(FileSearchBindingModel model)
        {
            try
            {
                var searchQuery = new FileSearchQuery
                {
                    Name = model.Name,
                    FolderId = model.FolderId,
                    IncludeAllFolders = model.IncludeAllFolders
                };
                var result = (from file in UnitOfWork.FileRepository.GetByName(searchQuery)
                              select new FileGetViewModel(file));

                return Ok(result);
            }
            catch(Exception ex)
            {
                Logger.LogError($"Unexpected error occured when search file by name with name {model.Name}! Exception details: {ex}");
                throw;
            }
        }

        /// <summary>
        /// Method that searches files by start wit specified string
        /// </summary>
        /// <param name="partOfName"></param>
        /// <returns></returns>
        [HttpGet("search")]
        public ActionResult<FileGetViewModel> Search(string partOfName)
        {
            try
            {
                var result = (from file in UnitOfWork.FileRepository.SearchByName(partOfName)
                              select new FileGetViewModel(file)).ToArray();

                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.LogError($"Unexpected error occured when search file by part of name {partOfName}! Exception details: {ex}");
                throw;
            }
        }
    }
}
