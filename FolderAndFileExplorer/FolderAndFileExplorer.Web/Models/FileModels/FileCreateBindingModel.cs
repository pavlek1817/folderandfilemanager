﻿using FolderAndFileExplorer.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FolderAndFileExplorer.Web.Models.FileModels
{
    public class FileCreateBindingModel
    {
        public string Name { get; set; }
        public int? FolderId { get; set; }

        public File GetFile(string filePath)
        {
            return new File
            {
                Name = Name,
                RelativePath = filePath,
                FolderId = FolderId,
            };
        }
    }
}
