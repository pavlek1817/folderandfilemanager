﻿using FolderAndFileExplorer.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FolderAndFileExplorer.Web.Models.FileModels
{
    public class FileGetViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public string FolderName { get; set; }

        public FileGetViewModel(File file)
        {
            Id = file.Id;
            Name = file.Name;
            CreateDate = file.CreateDate;
            FolderName = file.Folder != null ? file.Folder.Name: string.Empty;
        }
    }
}
