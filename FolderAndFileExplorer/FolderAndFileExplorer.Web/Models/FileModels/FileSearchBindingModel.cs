﻿namespace FolderAndFileExplorer.Web.Models.FileModels
{
    public class FileSearchBindingModel
    {
        public int? FolderId { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// This flags indicates if all folders should be searched
        /// </summary>
        public bool IncludeAllFolders { get; set; }
    }
}
