﻿using FolderAndFileExplorer.Core.Entities;

namespace FolderAndFileExplorer.Web.Models.FolderModels
{
    public class FolderCreateBindingModel
    {
        public string Name { get; set; }
        public int? FolderId { get; set; }

        public Folder GetFolder(string folderPath)
        {
            return new Folder
            {
                Name = Name,
                Path = folderPath,
                ParentId = FolderId,
            };
        }
    }
}
