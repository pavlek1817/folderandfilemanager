﻿using FolderAndFileExplorer.Core.Entities;
using System;

namespace FolderAndFileExplorer.Web.Models.FolderModels
{
    public class FolderGetViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }

        public FolderGetViewModel(Folder folder)
        {
            Id = folder.Id;
            Name = folder.Name;
            CreateDate = folder.CreateDate;
        }
    }
}
