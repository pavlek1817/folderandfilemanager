using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using FolderAndFileExplorer.BusinessLogic;
using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.BusinessLogic;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using FolderAndFileExplorer.DataAccess;
using FolderAndFileExplorer.DataAccess.Repositories;
using FolderAndFileExplorer.Web.Mappings;
using FolderAndFileExplorer.Web.Models.FileModels;
using FolderAndFileExplorer.Web.Validators.FileModelsValidator;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace FolderAndFileExplorer.Web
{
    public class Startup
    {
        private static IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Latest)
                .AddFluentValidation();

            services.AddControllers();

            // Add swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "File and folder test api",
                    Version = "v1"
                });
            });

            // Add Auto Mapper Configurations
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            // Add database
            services.AddDbContextPool<FolderAndFileDbContext>(options => 
                options.UseSqlServer(_configuration.GetConnectionString("FolderAndFileDbContext")));

            // Initialize business logic
            services.AddScoped<IFileAndFolderBusinessLogic, FileAndFolderBusinesLogic>();

            // Initialize repositories and db context
            services.AddScoped<IFolderAndFileDbContext, FolderAndFileDbContext>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IFolderRepository, FolderRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Initialize validators
            services.AddTransient<IValidator<FileCreateBindingModel>, FileCreateValidator>();
            services.AddTransient<IValidator<FileSearchBindingModel>, FileSearchValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            // app.UseHttpsRedirection();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapHub
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My Awesome API V1");
                });
            }

            
        }
    }
}
