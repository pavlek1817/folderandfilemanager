﻿using FluentValidation;
using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Web.Models.FolderModels;

namespace FolderAndFileExplorer.Web.Validators.FolderModelsValidator
{
    public class FolderCreateValidator : AbstractValidator<FolderCreateBindingModel>
    {
        public FolderCreateValidator(IUnitOfWork unitOfWork)
        {
            // If folder is not null, then check if it exits
            When(x => x.FolderId.HasValue, () =>
            {
                RuleFor(x => x.FolderId).Must(x => unitOfWork.FolderRepository.CheckIfExists(x.Value))
                    .WithMessage($"Specified folder doesn't exists!");

                // Check if folder with same name exists in specified folder
                RuleFor(x => x).Must(x => !unitOfWork.FolderRepository.CheckIfExists(x.Name, x.FolderId))
                        .WithMessage(x => $"Folder with specified name {x.Name} already exists in folder with id {x.FolderId}!");

            }).Otherwise(() => {

                // Check if folder with same name exists in root folder
                RuleFor(x => x).Must(x => !unitOfWork.FolderRepository.CheckIfExists(x.Name, null))
                        .WithMessage(x => $"Folder with specified name {x.Name} already exists in root folder!");
            });
        }
    }
}
