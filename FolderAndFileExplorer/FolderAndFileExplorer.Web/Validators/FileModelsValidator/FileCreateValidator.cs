﻿using FluentValidation;
using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Web.Models.FileModels;

namespace FolderAndFileExplorer.Web.Validators.FileModelsValidator
{
    public class FileCreateValidator : AbstractValidator<FileCreateBindingModel>
    {
        public FileCreateValidator(IUnitOfWork unitOfWork)
        {

            // Check if file is not empty string
            RuleFor(x => x.Name).NotEmpty().WithMessage("File name has to be non empty string!");

            // If folder is not null, then check if it exits
            When(x => x.FolderId.HasValue, () =>
            {
                
                // Check if folder exists
                RuleFor(x => x.FolderId)
                    .Must(x => unitOfWork.FolderRepository.CheckIfExists(x.Value))
                    .WithMessage($"Specified folder id doesn't exists!");

                // Check if file exists in specified folder
                RuleFor(x => x).Must(x => !unitOfWork.FileRepository.CheckIfExists(x.Name, x.FolderId))
                        .WithMessage(x => $"File with specified name {x.Name} already exists in folder with id {x.FolderId}!");

            }).Otherwise(() =>
            {
                // Check if file exists in specified folder
                RuleFor(x => x).Must(x => !unitOfWork.FileRepository.CheckIfExists(x.Name, null))
                    .WithMessage(x => $"File with specified name {x.Name} already exists in root folder!");
            });
        }

    }
}
