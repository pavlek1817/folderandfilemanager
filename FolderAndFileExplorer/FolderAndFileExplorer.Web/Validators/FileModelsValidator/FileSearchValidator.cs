﻿using FluentValidation;
using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.DataAccess;
using FolderAndFileExplorer.Web.Models.FileModels;

namespace FolderAndFileExplorer.Web.Validators.FileModelsValidator
{
    public class FileSearchValidator : AbstractValidator<FileSearchBindingModel>
    {
        public FileSearchValidator(IUnitOfWork unitOfWork)
        {
            // If folder is not null, then check if it exits
            When(x => x.FolderId.HasValue, () =>
            {
                RuleFor(x => x.FolderId).Must(x => unitOfWork.FolderRepository.CheckIfExists(x.Value))
                    .WithMessage($"Specified folder doesn't exists!");
            });
        }
    }
}
