﻿using FolderAndFileExplorer.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FolderAndFileExplorer.DataAccess.EntityConfigurations
{
    internal class FolderConfiguration : IEntityTypeConfiguration<Folder>
    {
        public void Configure(EntityTypeBuilder<Folder> builder)
        {
            builder.ToTable("Folder", schema: "dbo");
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Parent)
                .WithMany(x => x.SubFolders)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
