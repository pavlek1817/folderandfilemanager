﻿using FolderAndFileExplorer.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FolderAndFileExplorer.DataAccess.EntityConfigurations
{
    internal class FileConfiguration : IEntityTypeConfiguration<File>
    {
        public void Configure(EntityTypeBuilder<File> builder)
        {
            builder.ToTable("File", schema: "dbo");
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Folder)
                .WithMany(x => x.Files)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
