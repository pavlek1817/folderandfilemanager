﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using FolderAndFileExplorer.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore.Storage;

namespace FolderAndFileExplorer.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private IFolderRepository _folderRepository;
        private IFileRepository _fileRepository;
        private readonly IFolderAndFileDbContext _dbContext;
        public UnitOfWork(IFolderAndFileDbContext dbContext, IFolderRepository folderRepository,
            IFileRepository fileRepository)
        {
            _dbContext = dbContext;
            _folderRepository = folderRepository;
            _fileRepository = fileRepository;
        }

        public IFolderRepository FolderRepository => _folderRepository;

        public IFileRepository FileRepository => _fileRepository;

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return _dbContext.BeginTransaction();
        }
    }
}
