﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using System;
using System.Linq;

namespace FolderAndFileExplorer.DataAccess.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity, new()
    {
        protected readonly IFolderAndFileDbContext DbContext;
        public Repository(IFolderAndFileDbContext dbContext)
        {
            DbContext = dbContext;
        }
        public virtual void Add(TEntity entity)
        {
            entity.CreateDate = DateTime.Now;
            DbContext.ModelSet<TEntity>().Add(entity);
        }

        public bool CheckIfExists(int id)
        {
            return DbContext.ModelSet<TEntity>().Any(x => x.Id == id);
        }

        public virtual void Delete(int id)
        {
            DbContext.ModelSet<TEntity>().Remove(GetById(id));
        }

        public virtual TEntity GetById(int id)
        {
            return DbContext.ModelSet<TEntity>().Find(id);
        }

        public void Update(TEntity entity)
        {
            entity.LastModified = DateTime.Now;
            DbContext.ModelSet<TEntity>().Update(entity);
        }

        protected IQueryable<TEntity> Fetch()
        {
            return DbContext.ModelSet<TEntity>();
        }
    }
}
