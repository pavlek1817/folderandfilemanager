﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using FolderAndFileExplorer.Core.Entities;
using FolderAndFileExplorer.Core.Queries;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FolderAndFileExplorer.DataAccess.Repositories
{
    public class FileRepository : Repository<File>, IFileRepository
    {
        public FileRepository(IFolderAndFileDbContext dbContext):base(dbContext)
        {

        }

        public override File GetById(int id)
        {
            return Fetch().Include(x => x.Folder)
                .Single(x => x.Id == id);
        }

        public ICollection<File> GetByName(FileSearchQuery query)
        {
            var dbSet = Fetch().Include(x => x.Folder).AsQueryable();

            // Check if all folders are included
            if (!query.IncludeAllFolders)
            {
                dbSet = dbSet.Where(x => x.FolderId == query.FolderId);
            }

            return dbSet.Where(x => x.Name == query.Name).ToArray();
        }

        public bool CheckIfExists(string fileName, int? folderId)
        {
            return Fetch().Where(x => x.FolderId == folderId && x.Name == fileName)
                .Any();
        }

        public ICollection<File> SearchByName(string partOfName)
        {
            return Fetch().Include(x => x.Folder)
                .Where(x => x.Name.StartsWith(partOfName))
                .OrderBy(x => x.Name)
                .Take(10).ToArray();
        }
    }
}
