﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using FolderAndFileExplorer.Core.Entities;
using FolderAndFileExplorer.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace FolderAndFileExplorer.DataAccess.Repositories
{
    public class FolderRepository : Repository<Folder>, IFolderRepository
    {
        public FolderRepository(IFolderAndFileDbContext dbContext)
            :base(dbContext)
        {

        }

        public override void Delete(int id)
        {
            // Get sub folders
            var subfolders = Fetch().Where(x => x.ParentId == id)
                .ToArray();

            // Get files
            var files = DbContext.ModelSet<File>().Where(x => x.FolderId == id)
                .ToArray();

            foreach(var subfolder in subfolders)
            {
                Delete(subfolder.Id);
            }

            DbContext.ModelSet<File>().RemoveRange(files);

            base.Delete(id);
        }

        public bool CheckIfExists(string name, int? parentId)
        {
            return Fetch().Where(x => x.ParentId == parentId && x.Name == name)
                .Any();
        }

        public ICollection<FileAndFolderGetViewModel> GetFolderItems(int? folderId)
        {
            var subfolders = (from folder in Fetch()
                              where folder.ParentId == folderId
                              orderby folder.Name
                              select new FileAndFolderGetViewModel(folder))
                              .ToList();

            var files = (from file in DbContext.ModelSet<File>()
                         where file.FolderId == folderId
                         orderby file.Name
                         select new FileAndFolderGetViewModel(file))
                         .ToList();

            return subfolders.Concat(files).ToArray();
        }
    }
}
