﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.DataAccess.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace FolderAndFileExplorer.DataAccess
{
    public class FolderAndFileDbContext : DbContext, IFolderAndFileDbContext
    {
        public FolderAndFileDbContext(DbContextOptions<FolderAndFileDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        
        public DbSet<TEntity> ModelSet<TEntity>() where TEntity: class, IEntity, new()
        {
            return Set<TEntity>();
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return Database.BeginTransaction();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.ApplyConfiguration(new FileConfiguration())
                .ApplyConfiguration(new FolderConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
