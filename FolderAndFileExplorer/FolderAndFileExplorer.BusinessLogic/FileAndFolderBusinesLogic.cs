﻿using FolderAndFileExplorer.Core.Contracts.BusinessLogic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Text;

namespace FolderAndFileExplorer.BusinessLogic
{
    public class FileAndFolderBusinesLogic : IFileAndFolderBusinessLogic
    {
        private readonly string _absolutePath;
        private readonly string _rootFolderName;
        private string RootFolderPath { 
            get
            {
                return _absolutePath + _rootFolderName;
            }
        }
        public FileAndFolderBusinesLogic(IHostEnvironment env, IConfiguration configuration)
        {
            _absolutePath = env.ContentRootPath;
            _rootFolderName = configuration["RootFolderPath"];

        }
        public void CreateFile(string relativePath)
        {
            using (FileStream fileStream = new FileStream(GetFullPath(relativePath), FileMode.Create, FileAccess.ReadWrite))
            {
                // Add some text to file    
                byte[] text = new UTF8Encoding(true).GetBytes("New Text File");
                fileStream.Write(text, 0, text.Length);
            }
        }

        public void DeleteFile(string relativePath)
        {
            string fullPath = GetFullPath(relativePath);
            if (CheckIfFileExists(fullPath))
            {
                File.Delete(fullPath);
            }
        }

        public void UpdateFile(string relativePath)
        {
            // First delete file
            DeleteFile(relativePath);
            // Then, create new file
            CreateFile(relativePath);
        }

        public void CreateFolder(string folderPath)
        {
            Directory.CreateDirectory(GetFullPath(folderPath));
        }

        public void DeleteFolder(string folderPath)
        {
            var fullPath = GetFullPath(folderPath);

            if (Directory.Exists(fullPath))
            {
                Directory.Delete(fullPath, true);
            }
            
        }

        public void RenameFolder(string folderPath, string newFolderName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that checks if file exists
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private bool CheckIfFileExists(string filePath)
        {
            return File.Exists(filePath);
        }

        private string GetFullPath(string relativePath)
        {
            return $"{RootFolderPath}{relativePath}";
        }
    }
}
