﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using FolderAndFileExplorer.Web.Models.FileModels;
using FolderAndFileExplorer.Web.Validators.FileModelsValidator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderAndFileExplorer.UnitTests.ValidatorTests
{
    [TestClass]
    public class FileCreateValidatorTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IFileRepository> _mockFileRepository;
        private readonly Mock<IFolderRepository> _mockFolderRepository;

        public FileCreateValidatorTest()
        {
            _mockFileRepository = new Mock<IFileRepository>();
            _mockFolderRepository = new Mock<IFolderRepository>();
            _mockUnitOfWork = new Mock<IUnitOfWork>();
        }

        private void SetUp()
        {
            _mockUnitOfWork.Setup(x => x.FileRepository).Returns(_mockFileRepository.Object);
            _mockUnitOfWork.Setup(x => x.FolderRepository).Returns(_mockFolderRepository.Object);
        }

        private FileCreateValidator GetValidator()
        {
            return new FileCreateValidator(_mockUnitOfWork.Object);
        }

        [TestMethod]
        public void FileCreateValidator_CaseFileNameEmpty_ShouldReturnFalse()
        {
            // Set repositories
            _mockFileRepository.Setup(x => x.CheckIfExists("", null)).Returns(false);
            SetUp();

            // Assert
            var validator = GetValidator();

            var bm = new FileCreateBindingModel
            {
                FolderId = null,
                Name = ""
            };

            // Act and assert
            Assert.IsFalse(validator.Validate(bm).IsValid);
        }

        [TestMethod]
        public void FileCreateValidator_RootFolderAndFileExists_ShouldReturnFalse()
        {
            // Set repositories
            _mockFileRepository.Setup(x => x.CheckIfExists("test", null)).Returns(true);
            SetUp();

            // Assert
            var validator = GetValidator();

            var bm = new FileCreateBindingModel
            {
                FolderId = null,
                Name = "test"
            };

            // Act and assert
            Assert.IsFalse(validator.Validate(bm).IsValid);
        }

        [TestMethod]
        public void FileCreateValidator_RootFolderAndFileNotExists_ShouldReturnTrue()
        {
            // Set repositories that file doesn't exists
            _mockFileRepository.Setup(x => x.CheckIfExists("test", null)).Returns(false);
            SetUp();

            // Assert
            var validator = GetValidator();

            var bm = new FileCreateBindingModel
            {
                FolderId = null,
                Name = "test"
            };

            // Act and assert
            Assert.IsTrue(validator.Validate(bm).IsValid);
        }

        [TestMethod]
        public void FileCreateValidator_NotRootFolderExistsAndFileExists_ShouldReturnFalse()
        {
            // Set repositories that file exists
            _mockFileRepository.Setup(x => x.CheckIfExists("test", 1)).Returns(true);
            _mockFolderRepository.Setup(x => x.CheckIfExists(1)).Returns(true);
            SetUp();

            // Assert
            var validator = GetValidator();

            var bm = new FileCreateBindingModel
            {
                FolderId = 1,
                Name = "test"
            };

            // Act and assert
            Assert.IsFalse(validator.Validate(bm).IsValid);
        }

        [TestMethod]
        public void FileCreateValidator_NotRootFolderDoesntExistsAndFileDoesntExists_ShouldReturnFalse()
        {
            // Set repositories that file exists
            _mockFileRepository.Setup(x => x.CheckIfExists("test", 1)).Returns(false);
            _mockFolderRepository.Setup(x => x.CheckIfExists(1)).Returns(false);
            SetUp();

            // Assert
            var validator = GetValidator();

            var bm = new FileCreateBindingModel
            {
                FolderId = 1,
                Name = "test"
            };

            // Act and assert
            Assert.IsFalse(validator.Validate(bm).IsValid);
        }

        [TestMethod]
        public void FileCreateValidator_NotRootFolderExistsAndFileDoesntExists_ShouldReturnFalse()
        {
            // Set repositories that file exists
            _mockFileRepository.Setup(x => x.CheckIfExists("test", 1)).Returns(false);
            _mockFolderRepository.Setup(x => x.CheckIfExists(1)).Returns(true);
            SetUp();

            // Assert
            var validator = GetValidator();

            var bm = new FileCreateBindingModel
            {
                FolderId = 1,
                Name = "test"
            };

            // Act and assert
            Assert.IsTrue(validator.Validate(bm).IsValid);
        }
    }
}
