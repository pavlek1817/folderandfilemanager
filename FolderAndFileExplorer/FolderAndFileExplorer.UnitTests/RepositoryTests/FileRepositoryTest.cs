﻿using FolderAndFileExplorer.Core.Contracts;
using FolderAndFileExplorer.Core.Contracts.Repositories;
using FolderAndFileExplorer.Core.Entities;
using FolderAndFileExplorer.Core.Queries;
using FolderAndFileExplorer.DataAccess.Repositories;
using FolderAndFileExplorer.UnitTests.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderAndFileExplorer.UnitTests.RepositoryTests
{
    [TestClass]
    public class FileRepositoryTest
    {
        private readonly Mock<IFolderAndFileDbContext> _mockDbContext;
        private IFileRepository _fileRepository;
        private List<File> sourceList;

        public FileRepositoryTest()
        {
            _mockDbContext = new Mock<IFolderAndFileDbContext>();
        }

        private void SetUp()
        {
            _fileRepository = new FileRepository(_mockDbContext.Object);
            
        }

        private void PrepareData()
        {
            sourceList = new List<File>
            {
                new File
                {
                    Name = "Test1",
                    FolderId = 1,
                    Id = 1,
                    RelativePath = "Folder/Test1"
                },
                new File
                {
                    Name = "Test2",
                    FolderId = null,
                    Id = 2,
                    RelativePath = "Folder/Test2"
                },
                new File
                {
                    Name = "Test3",
                    FolderId = 3,
                    Id = 3,
                    RelativePath = "Folder/Test3"
                },
                new File
                {
                    Name = "Test3",
                    FolderId = null,
                    Id = 4,
                    RelativePath = "/"
                }
            };
        }

        [TestMethod]
        public void GetByName_OrdinaryCase_ShouldReturnCorrectResult()
        {
            PrepareData();
            _mockDbContext.Setup(x => x.ModelSet<File>()).Returns(sourceList.GetQueryableMockDbSet<File>());
            SetUp();

            var results = _fileRepository.GetByName(new Core.Queries.FileSearchQuery
            {
                FolderId = 1,
                Name = "Test1"
            });

            var result = results.First();

            Assert.AreEqual(1, results.Count);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("Test1", result.Name);
        }

        [TestMethod]
        public void GetByName_EmptyResultCase_ShouldReturnEmptyList()
        {
            PrepareData();
            _mockDbContext.Setup(x => x.ModelSet<File>()).Returns(sourceList.GetQueryableMockDbSet<File>());
            SetUp();

            var results = _fileRepository.GetByName(new FileSearchQuery
            {
                FolderId = null,
                Name = "Test1",
                IncludeAllFolders = false
            });

            Assert.AreEqual(0, results.Count);
        }

        [TestMethod]
        public void GetByName_IncludedAllFoldersCase_ShouldReturnTwoItems()
        {
            PrepareData();
            _mockDbContext.Setup(x => x.ModelSet<File>()).Returns(sourceList.GetQueryableMockDbSet<File>());
            SetUp();

            var results = _fileRepository.GetByName(new FileSearchQuery
            {
                FolderId = null,
                Name = "Test3",
                IncludeAllFolders = true
            });

            Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public void GetByName_NotIncludedAllFoldersCase_ShouldReturnOneItem()
        {
            PrepareData();
            _mockDbContext.Setup(x => x.ModelSet<File>()).Returns(sourceList.GetQueryableMockDbSet<File>());
            SetUp();

            var results = _fileRepository.GetByName(new FileSearchQuery
            {
                FolderId = null,
                Name = "Test3",
                IncludeAllFolders = false
            });

            Assert.AreEqual(1, results.Count);
        }
    }
}
