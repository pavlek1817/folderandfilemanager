﻿using FolderAndFileExplorer.Core.Contracts;
using System;

namespace FolderAndFileExplorer.Core.Entities
{
    public class File : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }

        #region Foreign keys
        public int? FolderId { get; set; }
        #endregion

        #region Lookup properties
        public Folder Folder { get; set; }
        #endregion

        #region System properties
        public DateTime CreateDate { get; set; }
        public DateTime? LastModified { get; set; }
        #endregion
    }
}
