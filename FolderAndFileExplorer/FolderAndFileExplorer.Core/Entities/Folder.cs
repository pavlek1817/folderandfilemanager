﻿using FolderAndFileExplorer.Core.Contracts;
using System;
using System.Collections.Generic;

namespace FolderAndFileExplorer.Core.Entities
{
    public class Folder : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }

        #region System properties
        public DateTime CreateDate { get; set; }
        public DateTime? LastModified { get; set; }
        #endregion

        #region Foreign keys
        public int? ParentId { get; set; }
        #endregion

        #region Lookup properties
        public Folder Parent { get; set; }
        #endregion

        #region Collections
        public ICollection<Folder> SubFolders { get; set; }
        public ICollection<File> Files { get; set; }
        #endregion

    }
}
