﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace FolderAndFileExplorer.Core.Contracts
{
    public interface IFolderAndFileDbContext
    {
        // Definition of model set for every type of entity
        public DbSet<TEntity> ModelSet<TEntity>() where TEntity : class, IEntity, new();

        // Save changes to the database
        public void SaveChanges();

        /// <summary>
        /// Method that gets transaction object
        /// </summary>
        /// <returns></returns>
        IDbContextTransaction BeginTransaction();

    }
}
