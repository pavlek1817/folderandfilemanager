﻿namespace FolderAndFileExplorer.Core.Contracts.BusinessLogic
{
    public interface IFileAndFolderBusinessLogic
    {
        #region File methods
        /// <summary>
        /// Method that creates file on specified file path for specified file in memory stream
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileStream"></param>
        void CreateFile(string filePath);

        /// <summary>
        /// Method that updates file on specified location, first removing existing file, then adding new one
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="memoryStream"></param>
        /// <returns></returns>
        void UpdateFile(string filePath);

        /// <summary>
        /// Method that deletes file on specified file path
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        void DeleteFile(string filePath);
        #endregion
        #region Folder methods
        /// <summary>
        /// Method that creates folder on the file system on specified path
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        void CreateFolder(string folderPath);
        /// <summary>
        /// Method that renames specified folde to the new name
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="newFolderName"></param>
        /// <returns></returns>
        void RenameFolder(string folderPath, string newFolderName);
        /// <summary>
        /// Method that deletes existing folder with all its content
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        void DeleteFolder(string folderPath);
        #endregion
    }
}
