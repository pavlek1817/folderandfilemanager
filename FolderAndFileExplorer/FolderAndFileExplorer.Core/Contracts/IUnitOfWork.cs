﻿using FolderAndFileExplorer.Core.Contracts.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace FolderAndFileExplorer.Core.Contracts
{
    public interface IUnitOfWork
    {
        IFolderRepository FolderRepository { get; }
        IFileRepository FileRepository { get; }

        /// <summary>
        /// Method that saves changes in the database
        /// </summary>
        void SaveChanges();
        /// <summary>
        /// Method that gets transaction object
        /// </summary>
        /// <returns></returns>
        IDbContextTransaction BeginTransaction();
    }
}
