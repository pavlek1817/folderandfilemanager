﻿using FolderAndFileExplorer.Core.Entities;
using FolderAndFileExplorer.Core.Models;
using System.Collections.Generic;

namespace FolderAndFileExplorer.Core.Contracts.Repositories
{
    public interface IFolderRepository : IRepository<Folder>
    {
        /// <summary>
        /// Method that checks if folder with specified name exists in target parent folder
        /// </summary>
        /// <param name="name">Folder's name</param>
        /// <param name="parentId">Parent folder's id, root if null</param>
        /// <returns></returns>
        bool CheckIfExists(string name, int? parentId);

        /// <summary>
        /// Method that gets all folder items, files and subfolders
        /// </summary>
        /// <param name="folderId">Specified folder id, root if folder id is null</param>
        /// <returns></returns>
        ICollection<FileAndFolderGetViewModel> GetFolderItems(int? folderId);
    }
}
