﻿using FolderAndFileExplorer.Core.Entities;
using FolderAndFileExplorer.Core.Queries;
using System.Collections.Generic;

namespace FolderAndFileExplorer.Core.Contracts.Repositories
{
    public interface IFileRepository : IRepository<File>
    {

        /// <summary>
        /// Method that search files for specified query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public ICollection<File> GetByName(FileSearchQuery query);
        /// <summary>
        /// Method that search top 10 files that starts with specified parameter
        /// </summary>
        /// <param name="partOfName"></param>
        /// <returns></returns>
        ICollection<File> SearchByName(string partOfName);
        /// <summary>
        /// Method that checks if file exists in specified folder
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="folderId"></param>
        /// <returns></returns>
        bool CheckIfExists(string fileName, int? folderId);
        
    }
}
