﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FolderAndFileExplorer.Core.Contracts.Repositories
{
    public interface IRepository<TEntity> where TEntity: class, IEntity, new()
    {
        /// <summary>
        /// Method that add new entity to the database
        /// </summary>
        /// <param name="entity"></param>
        void Add(TEntity entity);
        /// <summary>
        /// Method that updates existing entity in the database
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);
        /// <summary>
        /// Method  that deletes entity in the database
        /// </summary>
        /// <param name="entity"></param>
        void Delete(int id);
        /// <summary>
        /// Method that gets entity by specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity GetById(int id);
        /// <summary>
        /// Method that checks if entity exists in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool CheckIfExists(int id);
    }
}
