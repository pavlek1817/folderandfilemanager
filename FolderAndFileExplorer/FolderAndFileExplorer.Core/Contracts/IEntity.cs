﻿using System;

namespace FolderAndFileExplorer.Core.Contracts
{
    public interface IEntity
    {
        int Id { get; set; }
        DateTime CreateDate { get; set; }
        DateTime? LastModified { get; set; }
    }
}
