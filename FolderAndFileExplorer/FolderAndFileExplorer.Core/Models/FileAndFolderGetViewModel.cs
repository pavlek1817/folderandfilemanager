﻿using FolderAndFileExplorer.Core.Entities;
using FolderAndFileExplorer.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderAndFileExplorer.Core.Models
{
    public class FileAndFolderGetViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public FileOrFolderEnum ItemType { get; set; }


        public FileAndFolderGetViewModel(Folder folder)
        {
            Id = folder.Id;
            Name = folder.Name;
            CreateDate = folder.CreateDate;
            ItemType = FileOrFolderEnum.Folder;
        }

        public FileAndFolderGetViewModel(File file)
        {
            Id = file.Id;
            Name = file.Name;
            CreateDate = file.CreateDate;
            ItemType = FileOrFolderEnum.File;
        }
    }
}
