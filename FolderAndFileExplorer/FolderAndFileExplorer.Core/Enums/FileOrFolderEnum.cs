﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderAndFileExplorer.Core.Enums
{
    public enum FileOrFolderEnum
    {
        File,
        Folder
    }
}
