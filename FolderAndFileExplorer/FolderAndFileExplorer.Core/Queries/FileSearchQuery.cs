﻿namespace FolderAndFileExplorer.Core.Queries
{
    public class FileSearchQuery
    {
        public string Name { get; set; }
        public int? FolderId { get; set; }
        public bool IncludeAllFolders { get; set; }
    }
}
